package interfaceImplementations;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import clientInterface.*;
import entity.Car;

public class ComputeTaxesAndPrices extends UnicastRemoteObject implements ComputeTaxes, Serializable {
	private static final long serialVersionUID = 1L;

	public ComputeTaxesAndPrices() throws RemoteException {
		super();
	}

	public double fee(Car car) throws RemoteException {		
		int sum = 8;
		
		if(car.getEngineSize() > 1600 && car.getEngineSize() < 2000)	sum = 18;
		if(car.getEngineSize() >= 2000 && car.getEngineSize() < 2600)	sum = 72;
		if(car.getEngineSize() >= 2600 && car.getEngineSize() < 3000)	sum = 144;
		if(car.getEngineSize() >= 3000)	sum = 290;
		
		return car.getEngineSize() * sum / 200;
	}

	public double price(Car car) throws RemoteException {
		return (2018 - car.getYear()) < 7 ? car.getPrice() - (2018 - car.getYear()) * car.getPrice() / 7 : 0;
	}

}
