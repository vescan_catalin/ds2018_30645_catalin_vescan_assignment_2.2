package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import interfaceImplementations.ComputeTaxesAndPrices;

public class ServerStart {

	public static void main(String[] args) {
		Registry registry;
		try {
			registry = LocateRegistry.createRegistry(8889);			
			registry.rebind("myCar", new ComputeTaxesAndPrices());

			System.out.println("Server is running...");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
