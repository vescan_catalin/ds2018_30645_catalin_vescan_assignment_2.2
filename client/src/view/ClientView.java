package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;

public class ClientView extends JFrame{
	private static final long serialVersionUID = 1L;
	private JTextField yearText;
	private JTextField engineText;
	private JTextField priceText;
	private JButton feeButton;
	private JButton checkPrice;
	
	public ClientView() {
		this.setTitle("Compute");
		this.setSize(600, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel welcome = new JLabel("Welcome!");
		welcome.setFont(new Font("Tahoma", Font.PLAIN, 20));
		welcome.setBounds(225, 50, 100, 50);
		panel.add(welcome);
		
		JLabel yearLabel = new JLabel("year");
		yearLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		yearLabel.setBounds(59, 139, 100, 50);
		panel.add(yearLabel);
		
		JLabel engineSize = new JLabel("engine size");
		engineSize.setFont(new Font("Tahoma", Font.PLAIN, 20));
		engineSize.setBounds(59, 210, 100, 50);
		panel.add(engineSize);
		
		JLabel priceLabel = new JLabel("price");
		priceLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		priceLabel.setBounds(59, 282, 100, 50);
		panel.add(priceLabel);
		
		yearText = new JTextField();
		yearText.setBounds(180, 150, 200, 30);
		panel.add(yearText);
		yearText.setColumns(10);
		
		engineText = new JTextField();
		engineText.setColumns(10);
		engineText.setBounds(180, 219, 200, 30);
		panel.add(engineText);
		
		priceText = new JTextField();
		priceText.setColumns(10);
		priceText.setBounds(180, 290, 200, 30);
		panel.add(priceText);
		
		feeButton = new JButton("check fee");
		feeButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		feeButton.setBounds(125, 390, 150, 30);
		panel.add(feeButton);
		
		checkPrice = new JButton("check price");
		checkPrice.setFont(new Font("Tahoma", Font.PLAIN, 15));
		checkPrice.setBounds(325, 390, 150, 30);
		panel.add(checkPrice);
	}
	
	public void checkFee(ActionListener listener) {
		feeButton.addActionListener(listener);
	}
	
	public void checkPrice(ActionListener listener) {
	checkPrice.addActionListener(listener);
	}
	
	public int getYear() {
		return Integer.parseInt(yearText.getText());
	}
	
	public int getEngineSize() {
		return Integer.parseInt(engineText.getText());
	}
	
	public double getPrice() {
		return Double.parseDouble(priceText.getText());
	}
	
	
}
