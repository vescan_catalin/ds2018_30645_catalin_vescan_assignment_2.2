package clientController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JOptionPane;
import clientInterface.ComputeTaxes;
import entity.Car;
import view.ClientView;

public class Controller {

	ClientView clientView = new ClientView();
	ComputeTaxes computeTaxes;
	
	public Controller() {
		openUI();	
	}
	
	public void callMethodFromServer() throws MalformedURLException, RemoteException, NotBoundException {		
		Registry registry = LocateRegistry.getRegistry("127.0.0.1", 8889);
		
		computeTaxes = (ComputeTaxes) registry.lookup("myCar");
	}
	
	public void openUI() {
		clientView.setVisible(true);
				
		clientView.checkFee(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				try {
					callMethodFromServer();

					int year = clientView.getYear();
					int engineSize = clientView.getEngineSize();
					double price = clientView.getPrice();
										
					Double fee = new Double(computeTaxes.fee(new Car(year, engineSize, price)));
					JOptionPane.showMessageDialog(null, fee, "Your fee", JOptionPane.INFORMATION_MESSAGE);
				} catch (RemoteException e) {
					e.printStackTrace();
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NotBoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		
		clientView.checkPrice(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				try {
					callMethodFromServer();

					int year = clientView.getYear();
					int engineSize = clientView.getEngineSize();
					double price = clientView.getPrice();
					
					Double sellPrice = new Double(computeTaxes.price(new Car(year, engineSize, price)));
					JOptionPane.showMessageDialog(null, sellPrice, "Your selling price", JOptionPane.INFORMATION_MESSAGE);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NotBoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	
}
