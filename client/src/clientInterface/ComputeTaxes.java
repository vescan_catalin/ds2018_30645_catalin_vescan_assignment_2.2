package clientInterface;

import java.rmi.*;

import entity.Car;

public interface ComputeTaxes extends Remote {
	public double fee(Car car) throws RemoteException;
	public double price(Car car) throws RemoteException;	
}
